const puppeteer = require('puppeteer');

const loadScrapData = async () => {
    const browser = await puppeteer.launch()
    const page    = await browser.newPage();

    await page.setDefaultNavigationTimeout(0);
    await page.goto('http://aiweb.solve8.co.id/auth');

    await page.type('#logUsername-inputEl', 'admin02');
    await page.type('#textfield-1010-inputEl', 'Loginadmin02');
    await page.click('#toolbar-1011-targetEl a');
    await page.waitForNavigation();
    await page.click('#treeview-1024-record-9');
    await page.waitForSelector('#ext-gen1244 > div')

    let categories = await page.evaluate(() => {
        return Array.from(document.querySelectorAll('#headercontainer-1060-innerCt .x-column-header-text')).map((element) => {
            return element.textContent;
        });
    });

    categories.shift();

    const data = await page.evaluate(() => {
        return Array.from(document.querySelectorAll('#gridview-1068-body .x-grid-row.x-grid-row-alt.x-grid-data-row')).map((tableRowElement) => {
            return Array.from(tableRowElement.querySelectorAll('td')).map((tableData) => {
                return tableData.textContent;
            });
        });
    });

    await browser.close();
    
    return {
        categories, data
    }
};

module.exports.scrapeAiweb = loadScrapData;