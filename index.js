const express = require('express');
const app     = express();
const scrapper = require('./utils/scrapper');
const PORT      = process.env.PORT || 3000;

app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    res.render('pages/index');
});

app.get('/scrape', (req, res) => {
    const inventoryData = new Promise((resolve, reject) => {
        scrapper
            .scrapeAiweb()
            .then(data => resolve(data));
    });

    Promise.all([inventoryData])
        .then(data => {
            res.json({
                categories: data[0].categories,
                inventories: data[0].data,
            }); 
        });
});
app.use('/', express.static('public'));

app.listen(PORT, () => {
    console.log('Server is running on port ' + PORT)
});
