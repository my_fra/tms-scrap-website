function startScrape() {
    fetch('/scrape')
        .then((res) => res.json())
        .then((response) => {
            const inventories = response.inventories;
            const tbodyElement = document.querySelector('tbody');
            const spinnerWrapper = document.querySelector('.spinner-wrapper');

            tbodyElement.innerHTML = '';
            inventories.forEach((inventoryRow) => {
                let tdElements = '';

                inventoryRow.forEach((inventoryData) => {
                    tdElements += `<td>${inventoryData}</td>`
                });

                tbodyElement.insertAdjacentHTML('beforeend',
                    `<tr>${tdElements}</tr>`);
            });

            spinnerWrapper.classList.add('d-none');
        })
}